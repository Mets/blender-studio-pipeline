# Blender Studio Pipeline

The complete collection of documents, add-ons, scripts and tools that make up the Blender Studio pipeline. Learn more at [studio.blender.org](https://studio.blender.org/pipeline/).

## Development Setup

Before checking out this repo, ensure that you have `git-lfs` installed and enabled (use `git lfs install` to verify this).
